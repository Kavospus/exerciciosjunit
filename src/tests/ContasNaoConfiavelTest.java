package tests;

import static org.junit.Assert.*;

import modelo.ContasNaoConfiavel;

import org.junit.Before;
import org.junit.Test;

public class ContasNaoConfiavelTest {
	ContasNaoConfiavel contaFalsa;
	@Before
	public void setUp() throws Exception {
		contaFalsa = new ContasNaoConfiavel();
	}

	@Test
	public void testCalculaQuadradoEqualsZero() {
		assertEquals(0.0,contaFalsa.calculaQuadrado(0),0.0001);
	}
	@Test
	public void testCalculaQuadradoEqualsLittlePositive() {
		assertEquals(4.0,contaFalsa.calculaQuadrado(2),0.0001);
	}
	@Test
	public void testCalculaQuadradoEqualsLittleNegative() {
		assertEquals(9.0,contaFalsa.calculaQuadrado(-3),0.0001);
	}
	@Test
	public void testCalculaQuadradoEqualsHugePositive() {
		assertEquals(81000000000000000000.0,contaFalsa.calculaQuadrado(9000000000.0),0.0001);
	}
	@Test
	public void testCalculaQuadradoEqualsHugeNegative() {
		assertEquals(81000000000000000000.0,contaFalsa.calculaQuadrado(-9000000000.0),0.0001);
	}

}
